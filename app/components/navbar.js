import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'  
import React, { useState, useEffect } from 'react'
import { useRouter } from 'expo-router'
import * as Font from 'expo-font';

const Navbar = () => {
  const router = useRouter()
  const [fontLoaded, setFontLoaded] = useState(false);
    useEffect(() => {
    async function loadFonts() {
      await Font.loadAsync({
        'Poppins-Regular': require('../../assets/fonts/Poppins-Regular.ttf'),
      });
      
      setFontLoaded(true);
    }

    loadFonts();
  }, []);

  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <TouchableOpacity style={styles.content} activeOpacity={0.5} onPress={() => {router.push('screens/inicio/inicio')}} onPressIn={() => changeColor('#F5FBFF')}>
          <Image style={styles.Icon} source={require('../../assets/icons/inicio.png')}/>
          {fontLoaded && (
          <>
          <Text style={styles.Text}>Início</Text>
          </>
          )}
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} activeOpacity={0.5} onPress={() => {router.push('screens/alertas/alertas')}}>
          <Image style={styles.Icon2} source={require('../../assets/icons/alertas.png')}/>
          {fontLoaded && (
          <>
          <Text style={styles.Text}>Alertas</Text>
          </>
          )}
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} activeOpacity={0.5} onPress={() => {router.push('screens/novo-pedido/novoPedido')}}>
          <Image style={styles.Icon} source={require('../../assets/icons/button-new.png')}/>
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} activeOpacity={0.5} onPress={() => {router.push('screens/pedidos/pedidos')}}>
          <Image style={styles.Icon3} source={require('../../assets/icons/pedidos.png')}/>
          {fontLoaded && (
          <>
          <Text style={styles.Text}>Pedidos</Text>
          </>
          )}
        </TouchableOpacity>

        <TouchableOpacity style={styles.content} activeOpacity={0.5} onPress={() => {router.push('screens/perfil/perfil')}}>
          <Image style={styles.Icon} source={require('../../assets/icons/cartao.png')}/>
          {fontLoaded && (
          <>
          <Text style={styles.Text}>Perfil</Text>
          </>
          )}
        </TouchableOpacity>

      </View>
    </View>
  )
}

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    backgroundColor: "#2E4052",  
    justifyContent: "center",
    position: "fixed",
    bottom: 0,
    flexShrink: 0,
    width: '100%'
  },
  content: {
    flexDirection: 'collumn',
    padding: 10,
    justifyContent: 'center'
  },
  Icon : {
    alignItems: "center", 
  },
  Text: {
    color: "#fff",
    fontFamily: "Poppins-Regular",
    textAlign: 'center'
  
  },
  Icon2 : {
    alignItems: "center",
    justifyContent: 'center',
    marginLeft: 6
  },
  Icon3 : {
    alignItems: "center",
    marginLeft: 12
  }, 
})

export default Navbar