import { View, Text, StyleSheet, TouchableOpacity, Image } from 'react-native'
import { useFonts, Poppins_600SemiBold } from '@expo-google-fonts/poppins'
import { useRouter } from 'expo-router'

const TopNavbar = ({ titulo }) => {
  const router = useRouter()
  const [fontLoaded] = useFonts({
    Poppins_600SemiBold,
})
  return (
    <View style={styles.main}>
      <View style={styles.leftContent}>
      {fontLoaded && (
        <><Text style={styles.h1}>{titulo}</Text></>
      )}
      </View>
      <TouchableOpacity activeOpacity={0.5} onPress={() => {router.push('screens/novo-pedido/carrinho')}}>
        <Image style={styles.img} source={require('../../assets/icons/cart.png')}/>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.5} onPress={() => {router.push('screens/opcoes/opcoes')}}>
        <Image style={styles.img} source={require('../../assets/icons/settings.png')}/>
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  main: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 16, 
    height: 80,
  },
  leftContent: {
    flex: 1,
  },
  h1:{
    fontSize: 33,
    fontFamily: "Poppins_600SemiBold",
    color: '#000',
    textAlign: 'left',
  },
  img: {
    marginLeft: 7
  }
})

export default TopNavbar