import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput,  } from "react-native";
import {Picker} from '@react-native-picker/picker';
import { Link } from "expo-router"
import React, { useState, useEffect } from "react";
import { router } from 'expo-router';
import * as Font from 'expo-font';
import api from './api/api';

export default function Page() {
  
  const [fontLoaded, setFontLoaded] = useState(false);
  useEffect(() => {
    async function loadFonts() {
      await Font.loadAsync({
        'Poppins-SemiBold': require('../assets/fonts/Poppins-SemiBold.ttf'),
        'Poppins-Regular': require('../assets/fonts/Poppins-Regular.ttf'),
      });
      
      setFontLoaded(true);
    }

    loadFonts();
  }, []);
const [email, setEmail] = useState(null)
const [password, setPassword] = useState(null)
const [type_user, setTipoUsuario] = useState(null)

async function handleLogin(event){
  event.preventDefault()   
  try{
      const dataLogin = {
        email, password, type_user
      }
      const {data} = await api.post('/login', dataLogin)

      if (response.status === 201) {
          alert('Login realizado com sucesso');
          
          sessionStorage.setItem('login', true)
          sessionStorage.setItem('jwt', data.token)
          router.replace('/login');
        } else {
          alert('Credenciais inválidas');
        } 
  }catch(error){
    alert('Erro no login')
  }
}
  return (
    <View style={styles.container}>
      <View style={styles.main}>
        <View>
          <View style={styles.title}>
            <Image style={styles.cardImg} source={require('../assets/icons/Card.png')}/>
            {fontLoaded && (          
            <><Text style={styles.h1}>Entrar</Text></>
            )}
          </View>
            <View style={styles.form}>
            {fontLoaded && (
              <><Text style={styles.label}>Email</Text>
              <TextInput style={styles.Input} value={email} onChangeText={(text) => {
                if (text.length <= 100) {
                  setEmail(text);
                }}} maxLength={100} placeholder='Escreva aqui...'/></>
            )}

            {fontLoaded && (
              <><Text style={styles.label}>Senha</Text>
              <TextInput style={styles.Input} value={password} onChangeText={(text) => {
                if (text.length <= 16) {
                  setPassword(text);
                }}} maxLength={16} secureTextEntry={true} placeholder='Senha aqui...'/></>
            )}

            {/* {fontLoaded && (
              <><Text style={styles.label}>Tipo de Usuário</Text>
              <Picker style={styles.picker} onValueChange={(value) => setTipoUsuario(value)} value={type_user}>
                <Picker.Item label="RH" value="RH" labelStyle={styles.optionLabel} />
                <Picker.Item label="Almoxarifado" value="Almoxarifado" labelStyle={styles.optionLabel} />
                <Picker.Item label="Comum" value="Comum" labelStyle={styles.optionLabel} />                                                
              </Picker></>
            )} */}

            {fontLoaded && (
              <><TouchableOpacity style={styles.submit} activeOpacity={0.5} onPress={handleLogin}>
                <Text style={styles.submitTxt}>Entrar</Text>
              </TouchableOpacity></>
            )}

              <View>
              {fontLoaded && (
                <><Text style={styles.cadOptions}>Não tem conta? <Link style={styles.cadOptionsLink} href={'/screens/cadastro/cadastro'}>Criar Conta</Link></Text></>
              )}
              </View>
            </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {    
    flex: 1,
    alignItems: "center",
    padding: 24,
    backgroundColor: "#2E4052"
  },
  main: {
    flex: 1,
    justifyContent: "center",
    maxWidth: 960,
    marginHorizontal: "auto",
  },
  title: {
    paddingTop: 80,
    paddingBottom: 0
  },
  cardImg: {
    alignSelf: 'center',
    width: 75,
    height: 85,
  },
  h1:{
    color: '#fff',
    fontSize: 36,
    fontFamily: "Poppins-SemiBold",
    textAlign: 'center',
    paddingBottom: 30
  },
  form: {
    paddingBottom: 100
  },
  label: {
    paddingTop: 12,
    paddingBottom: 3,
    fontSize: 16,
    fontFamily: "Poppins-Regular",
    color: '#fff'
  },
  picker: {
    padding: 17,
    backgroundColor: "#FFE9BC",
    color: "#2E4052",
    fontFamily: "Poppins-Regular",
    borderStyle: "solid",
    borderColor: '#FFE9BC',
    borderRadius: 8,
    fontSize: 16,  
    borderRadius: 8,      
  },
  optionLabel: {
    fontFamily: "Poppins-Regular",
    fontSize: 16,  
  },
  Input: {
    padding: 17,
    backgroundColor: "#FFE9BC",
    color: "#2E4052",
    fontFamily: "Poppins-Regular",
    borderStyle: "solid",
    borderRadius: 8,
    fontSize: 16,
  },
  submit: {
    marginTop: 30,
    backgroundColor: "#FFC857",
    padding: 10,
    borderRadius: 8,
    fontSize: 16,
    fontFamily: "Poppins-Regular",
    color: '#fff',
    borderStyle: "solid",
    textAlign: 'center',
  },
  submitTxt: {
    fontSize: 18,
    color: '#fff', 
    textAlign: 'center', 
    fontFamily: 'Poppins-SemiBold'
  },
  cadOptions: {
    color: '#fff',
    textAlign: 'center',
    paddingTop: 22,
    paddingBottom: 35,
    paddingTop: 135,
    fontSize: 16,
    fontFamily: "Poppins-Regular",
  },
  cadOptionsLink: {
    color: "#FFC857",
  },
});
