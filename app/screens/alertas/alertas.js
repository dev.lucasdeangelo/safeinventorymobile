import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import React, { useEffect, useState } from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import { useRouter } from 'expo-router'
import * as Font from 'expo-font';

const alertas = () => {
  const router = useRouter()
  const [fontLoaded, setFontLoaded] = useState(false);
    useEffect(() => {
    async function loadFonts() {
      await Font.loadAsync({
        'Poppins-SemiBold': require('../../../assets/fonts/Poppins-SemiBold.ttf'),
        'Poppins-Regular': require('../../../assets/fonts/Poppins-Regular.ttf'),
      });
      
      setFontLoaded(true);
    }

    loadFonts();
  }, []);
  return (
    <View style={{ backgroundColor: '#F3F3F3', height: '90%' }}>
      <TopNavbar titulo='Alertas'/>
      <View style={styles.container}>
        <TouchableOpacity onPress={() => {router.push('screens/pedidos/pedidos')}}>
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/Ellipse5.png')}/>
            <View style={styles.content}>
            {fontLoaded && (
              <>
              <Text style={styles.h4}>Retire seu EPI Agora!</Text>
              <Text style={styles.subTitle}>Botas 43</Text>
              <Text style={{...styles.subTitle, color: '#B7B7B7'}}>Botas</Text>            
              </>
            )}
            </View>
            <View>
            {fontLoaded && (
              <>
              <Text style={styles.hora}>11:11</Text>
              </>
            )}
            </View>
          </View>
        </TouchableOpacity>
      </View>
      <Navbar/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
    marginHorizontal: 15,
    marginVertical: 10,
    borderRadius: 10,
    elevation: 7,
    shadowColor: '#C7C7C7',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 7,
  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  img: {
    marginLeft: 15,
    marginTop: 10
  },
  content: {
    paddingRight: 55,
    marginTop: 10,
  },
  h4: {
    fontFamily: 'Poppins-SemiBold',
  },
  subTitle: {
    fontFamily: 'Poppins-Regular',
  },
  hora: {
    fontFamily: 'Poppins-Regular',
    paddingRight: 20
  },
})

export default alertas