import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native'
import React from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import { useFonts, Poppins_600SemiBold, Poppins_500Medium, Poppins_400Regular,  } from '@expo-google-fonts/poppins'

const perfil = () => {
  const [fontLoaded] = useFonts({
    Poppins_600SemiBold,
    Poppins_500Medium,
    Poppins_400Regular
})
  return (
    <View style={{ backgroundColor: '#F3F3F3', height: '90%' }}>
      <TopNavbar titulo='Perfil'/>
      <View style={styles.container}>
        <View style={{...styles.container, ...styles.main}}>
          <Image style={styles.img} source={require('../../../assets/images/Ellipse5.png')}/>
        </View>
        <View style={styles.title}>
          <Text style={styles.h2}>Arnan</Text>
        </View>
        <Text style={styles.h3}>Pista Oeste</Text>
        <Text style={{...styles.h2, ...styles.title}}>Informações Pessoais</Text>
          <View style={{flexDirection: 'column'}}>
            <View style={styles.title}>
              <Text style={styles.h3}>Email: </Text>
              <Text style={styles.h4}>Arnan@gmail.com</Text>
              <TouchableOpacity>
                <Image source={require('../../../assets/icons/edit.png')}/>
              </TouchableOpacity>
            </View>
            <View style={styles.title}>
              <Text style={styles.h3}>Senha: </Text>
              <Text style={styles.h4}>****************</Text>
              <TouchableOpacity>
                <Image source={require('../../../assets/icons/edit.png')}/>
              </TouchableOpacity>
            </View>
          </View>
      </View>
      <Navbar/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
    marginHorizontal: 15,
    marginVertical: 10,
    borderRadius: 10,
    elevation: 7,
    shadowColor: '#C7C7C7',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 7,
  },
  main: {
    backgroundColor: '#FFC857', 
    height: '20%',
    flexDirection: 'column'
  },
  img: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: '12vh',
  },
  title: {
    justifyContent:'center', 
    alignItems: 'center', 
    flexDirection: 'row',
    paddingTop: '3vh',
  },
  h2: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: "Poppins_600SemiBold",
  },
  h3: {
    textAlign: 'center',
    fontSize: 16,
    fontFamily: "Poppins_500Medium",
  },
  h4: {
    textAlign: 'center',
    fontSize: 14,
    fontFamily: "Poppins_400Regular",
  },
})

export default perfil