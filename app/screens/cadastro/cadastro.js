import { View, Image, Text, StyleSheet, TextInput, TouchableOpacity, Pressable, Platform } from 'react-native'
import React, { useState, useEffect  } from 'react'
import { Link } from 'expo-router'
import * as Font from 'expo-font';
import api from '../../api/api';
import { Picker } from '@react-native-picker/picker';
import DateTimePicker from '@react-native-community/datetimepicker';


    const cadastro = () => {
        const [fontLoaded, setFontLoaded] = useState(false);
        useEffect(() => {
        async function loadFonts() {
        await Font.loadAsync({
            'Poppins-SemiBold': require('../../../assets/fonts/Poppins-SemiBold.ttf'),
            'Poppins-Regular': require('../../../assets/fonts/Poppins-Medium.ttf'),
        });
        
        setFontLoaded(true);
        }

        loadFonts();
    }, []);
    
        const [name_employee, setNome] = useState('')
        const [cpf_employee, setCpf] = useState('')
        const [email, setEmail] = useState('')
        const [password, setPassword] = useState('')
        const [birth_employee, setBirth] = useState(new Date());
        const [name_function, setFunction] = useState('')
        const [showDatePicker, setShowDatePicker] = useState(false);
        
        const toggleDatepicker = () => {
            setShowDatePicker(!showDatePicker)
        }

        const onChange = ({ type }, selectedDate) => {
            if (type == "set"){
                const currentDate = selectedDate
                setBirth(currentDate)

                if (Platform.OS === "android"){
                    toggleDatepicker()
                    setBirth(currentDate.toDateString())
                }
            }else {
                toggleDatepicker()
            }
        }
        
        async function handleRegister(event){   
            try{
                const data = {
                    name_employee, cpf_employee, email, password, birth_employee, name_function 
                }
                const response = await api.post('/employee', data)

                if (response.status === 201) {
                    alert('Cadastro realizado com sucesso');
                    setNome('');
                    setCpf('');
                    setEmail('');
                    setPassword('');
                    setBirth('');
                    setFunction('');
                } else {
                    alert('Erro ao cadastrar usuário');
                } 
            }catch(error){
                alert('Erro ao cadastrar usuário')
            }
        }
    return (
        <View style={styles.container}>
        <View style={styles.main}>
            <View>
                <View style={styles.title}>
                    <Image style={styles.cardImg} source={require('../../../assets/icons/Card.png')}/>
                    {fontLoaded && (                  
                    <>
                        <Text style={styles.h1}>Cadastro</Text>
                    </>
                    )}                
                </View>
                <View style={styles.form}>                
                    {fontLoaded && (
                    <>
                        <Text style={styles.label}>Nome</Text>
                        <TextInput style={styles.Input} value={name_employee} onChangeText={(text) => {
                            if (text.length <= 150) {
                                setNome(text);
                            }
                        }} maxLength={150} placeholder='Escreva aqui...'/>
                    </>
                    )}

                    {fontLoaded && (
                    <>
                        <Text style={styles.label}>CPF</Text>
                        <TextInput style={styles.Input} value={cpf_employee} onChangeText={(text) => {
                            if (text.length <= 14) {
                                setCpf(text);
                            }
                        }} maxLength={14} placeholder='Escreva aqui...'/>                     
                    </>
                    )}

                    {fontLoaded && (
                    <>
                        <Text style={styles.label}>Email</Text>
                        <TextInput style={styles.Input} value={email} onChangeText={(text) => {
                            if (text.length <= 100) {
                                setEmail(text);
                            }
                        } }maxLength={100} placeholder='Escreva aqui...'/>
                        </>
                    )}

                    {fontLoaded && (
                    <>
                        <Text style={styles.label}>Senha</Text>
                        <TextInput style={styles.Input} value={password} onChangeText={(text) => {
                            if (text.length <= 16) {
                                setPassword(text);
                            }
                        }} maxLength={16} secureTextEntry={true} placeholder='Senha aqui...'/>
                    </>
                    )}

                    {fontLoaded && (
                        <>
                            <View style={styles.datePickerContainer}>
                                <Text style={styles.label}>Data de Nascimento</Text>
                                {showDatePicker &&(
                                    <DateTimePicker
                                        mode='date'
                                        display='spinner'                             
                                        onChange={onChange}
                                        value={birth_employee}
                                        style={styles.datePicker}
                                    />
                                )}
                                    {showDatePicker && Platform.OS === 'ios' && (

                                        <View style={{ flexDirection: 'row', justifyContent: 'space-around'}}>
                                           <TouchableOpacity style={[styles.submit]}>
                                                <Text>Cancelar</Text>
                                            </TouchableOpacity> 
                                        </View>
                                    )}

                                {!showDatePicker && (
                                    <Pressable onPress={toggleDatepicker}>
                                        <TextInput 
                                        style={styles.Input} 
                                        value={birth_employee} 
                                        placeholder='Selecione a Data...' 
                                        onChangeText={setBirth} 
                                        editable={false} 
                                        onPressIn={toggleDatepicker}/>
                                    </Pressable>
                                )}
                                
                            </View>
                        </>
                    )}

                    {fontLoaded && (
                    <>
                        <Text style={styles.label}>Qual é a sua Função?</Text>
                    
                        <Picker style={styles.picker} selectedValue={name_function} onValueChange={(value) => setFunction(value)}>
                            <Picker.Item label="Conferente" value="Conferente" labelStyle={styles.optionLabel}/>
                            <Picker.Item label="Pista Leste" value="Almoxarifado" labelStyle={styles.optionLabel}/>
                            <Picker.Item label="Carga Leste" value="Carga Leste" labelStyle={styles.optionLabel}/>                                                
                        </Picker>
                    </>
                 )}

                {fontLoaded && (
                    <>
                        <TouchableOpacity style={styles.submit} activeOpacity={0.5} onPress={handleRegister}>
                            <Text style={styles.submitTxt}>Cadastrar</Text>
                        </TouchableOpacity>
                    </>
                )}
                {fontLoaded && (
                    <>
                        <Text style={styles.logOptions}>Já tem conta? <Link style={styles.logOptionsLink} href={'/'}>Entrar</Link></Text>
                    </>
                )}
                </View>
                <View>
                {fontLoaded && (
                    <>
                        <Text style={styles.cadText}>Ao se cadastrar você estará concordando com os termos de politica e privacidade!</Text>
                    </>
                )}
                </View>
            </View>
        </View>
        </View>
    )
    }

    const styles = StyleSheet.create({
        container: {
        flex: 1,
        alignItems: "center",
        padding: 24,
        backgroundColor: "#2E4052"
        },
        main: {
        flex: 1,
        justifyContent: "center",
        maxWidth: 960,
        marginHorizontal: "auto",
        },
        title: {
            paddingBottom: 0,
            paddingTop: 80,
        },
        cardImg: {
            left: '40%',
            position: 'relative',
            width: 75,
            height: 85,
        },
        h1:{
            fontSize: 36,
            fontFamily: "Poppins-SemiBold",
            color: '#fff',
            textAlign: 'center',
        },
        form: {
            paddingBottom: 100
        },
        label: {
            paddingTop: 12,
            paddingBottom: 3,
            fontSize: 16,
            fontFamily: "Poppins-Regular",
            color: '#fff'
        },
        Input: {        
            padding: 17,
            backgroundColor: "#FFE9BC",
            color: "#2E4052",
            fontFamily: "Poppins-Regular",
            borderStyle: "solid",
            borderColor: '#FFE9BC',
            borderRadius: 8,
            fontSize: 16,
        },
        datePickerContainer: {
            paddingTop: 12,
        },
        pickerView: {
            fontFamily: "Poppins-Regular",
        },
        picker: {
            padding: 17,
            backgroundColor: "#FFE9BC",
            color: "#2E4052",
            fontFamily: "Poppins-Regular",
            borderStyle: "solid",
            borderColor: '#FFE9BC',
            borderRadius: 8,
            fontSize: 16,        
        },
        optionLabel: {
            fontFamily: "Poppins-Regular",
            fontSize: 16,  
        },
        datePicker: {
            height: 120,
            marginTop: -10
        },
        submit: {
            marginTop: 30,
            backgroundColor: "#FFC857",
            padding: 14,
            borderRadius: 8,
            fontSize: 16,
            fontFamily: "Poppins-Regular",
            borderStyle: "solid",
            textAlign: 'center'
        },
        submitTxt: {
            fontSize: 18,
            color: '#fff', 
            textAlign: 'center', 
            fontFamily: 'Poppins-Regular'
        },
        logOptions: {
            color: '#fff',
            textAlign: 'center',
            paddingTop: 42,
            paddingBottom: 3,
            fontSize: 16,
            fontFamily: "Poppins-Regular",
        },
        logOptionsLink: {
            color: "#FFC857"
        },
        cadText: {
            fontSize: 14,
            fontFamily: "Poppins-Regular",
            color: '#fff',
            textAlign: 'center'
        },

    })
    export default cadastro