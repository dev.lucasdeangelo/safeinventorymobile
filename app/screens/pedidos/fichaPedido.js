import { View, Image, Text, TouchableOpacity, StyleSheet } from 'react-native'
import React from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import { useRouter } from 'expo-router'
import { useFonts, Poppins_600SemiBold, Poppins_500Medium, Poppins_400Regular,  } from '@expo-google-fonts/poppins'

const pedidos = () => {
  const router = useRouter()
  const [fontLoaded] = useFonts({
    Poppins_600SemiBold,
    Poppins_500Medium,
    Poppins_400Regular
})
  return (
    <View style={{ backgroundColor: '#F3F3F3', height: '90%' }}>
      <TopNavbar titulo='Pedido #0001'/>
      <View style={styles.container}>
      <TouchableOpacity onPress={() => {router.push('screens/pedidos/pedidos')}}>
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/Ellipse5.png')}/>
            <View style={styles.content}>
              <Text style={styles.h4}>Bota</Text>
              <Text style={styles.subTitle}>Botas Tam 42</Text>
              <Text style={{...styles.subTitle, color: '#B7B7B7'}}>1x</Text>            
            </View>
            <View>
              <Text style={styles.hora}>11:11</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      <Navbar/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
    marginHorizontal: 15,
    marginVertical: 10,
    borderRadius: 10,
    elevation: 7,
    shadowColor: '#C7C7C7',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 7,
  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  img: {
    marginLeft: 15,
    marginTop: 10
  },
  content: {
    paddingRight: 95,
    marginTop: 10,
  },
  h4: {
    fontFamily: 'Poppins_500Medium',
  },
  subTitle: {
    fontFamily: 'Poppins_400Regular',
  },
  hora: {
    fontFamily: 'Poppins_400Regular',
    paddingRight: 20,
  },
})

export default pedidos