import { StyleSheet, Image, Text, View, TouchableOpacity } from 'react-native'
import React from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import { useRouter } from 'expo-router'
import { useFonts, Poppins_500Medium, Poppins_400Regular } from '@expo-google-fonts/poppins'

const carrinho = () => {
  const router = useRouter()
  const [fontLoaded] = useFonts({
    Poppins_500Medium,
    Poppins_400Regular
})
  return (
    <View style={{ backgroundColor: '#F3F3F3', height: '90%' }}>
      <TopNavbar titulo='Carrinho'/>
      <View style={styles.container}>
      <TouchableOpacity onPress={() => {router.push('screens/pedidos/fichaPedido')}}>
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/Ellipse5.png')}/>
            <View style={styles.content}>
              <Text style={styles.h4}>Pedido #0001</Text>
              <Text style={styles.subTitle}>Bota</Text>
              <Text style={{...styles.subTitle, color: '#B7B7B7'}}>Botas Tam 42</Text>            
            </View>
            <View>
              <Text style={styles.hora}>11:11</Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.buttonsCart}>
        <View>
            <TouchableOpacity style={styles.button}>
                <Text style={{ color: '#fff', fontFamily: 'Poppins_400Regular'}}>Cancelar Pedido</Text>
            </TouchableOpacity>
        </View>
        <View>
            <TouchableOpacity style={{...styles.button, backgroundColor: '#3BD704', marginLeft: 10}}>
                <Text style={{ color: '#fff', fontFamily: 'Poppins_400Regular'}}>Realizar Pedido</Text>
            </TouchableOpacity>
        </View>
      </View>
      <Navbar/>
    </View>
  )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#fff',
        marginHorizontal: 15,
        marginVertical: 10,
        borderRadius: 10,
        elevation: 7,
        shadowColor: '#C7C7C7',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity: 0.7,
        shadowRadius: 7,
    },
    card: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    img: {
        marginLeft: 15,
        marginTop: 10
    },
    content: {
        paddingRight: 90,
        marginTop: 10,
    },
    h4: {
        fontFamily: 'Poppins_500Medium',
    },
    subTitle: {
        fontFamily: 'Poppins_400Regular',
    },
    hora: {
        fontFamily: 'Poppins_400Regular',
        paddingRight: 20,
    },
    buttonsCart: {
        flexDirection: 'row', 
        justifyContent: 'flex-end', 
        paddingRight: 15
    },
    button: {
        textAlign: 'center',
        justifyContent: 'center',
        color: '#fff',
        height: 40,
        width: 140,
        backgroundColor: '#FE0000',
        borderRadius: 5
    }
})

export default carrinho