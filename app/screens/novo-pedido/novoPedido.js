import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native'
import React from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import { useRouter } from 'expo-router'
import { useFonts, Poppins_500Medium, Poppins_400Regular } from '@expo-google-fonts/poppins'

const novoPedido = () => {
  const router = useRouter()
  const [fontLoaded] = useFonts({
    Poppins_500Medium,
    Poppins_400Regular
})
  return (
    <View style={{ backgroundColor: '#F3F3F3', height: '90%' }}>
      <TopNavbar titulo='Novo Pedido'/>
      <View style={style.row1}>
        <View style={style.container}>
          <View style={style.card}>
            <Image style={style.img} source={require('../../../assets/images/capacete.png')}/>
            <Text style={{...style.subTitle, color: '#3BD704', fontSize: 11}}>Estoque Disponível!</Text>
            <Text style={{...style.subTitle, fontFamily: 'Poppins_500Medium', fontSize: 16}}>Capacete</Text>
            <Text style={{...style.subTitle, color: '#B7B7B7'}}>Capacete Amarelo Tam 10</Text>
            <TouchableOpacity onPress={() => {router.push('screens/novo-pedido/carrinho')}}>
              <Text style={style.button}>Fazer pedido +</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={style.container}>
          <View style={style.card}>
            <Image style={style.img} source={require('../../../assets/images/luva.png')}/>
            <Text style={{...style.subTitle, color: '#3BD704', fontSize: 11}}>Estoque Disponível!</Text>
            <Text style={{...style.subTitle, fontFamily: 'Poppins_500Medium', fontSize: 16}}>Luva</Text>
            <Text style={{...style.subTitle, color: '#B7B7B7'}}>Luva de Borracha Cinza Tam M</Text>
            <TouchableOpacity onPress={() => {router.push('screens/novo-pedido/carrinho')}}>
              <Text style={style.button}>Fazer pedido +</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={style.container}>
          <View style={style.card}>
            <Image style={style.img} source={require('../../../assets/images/oculos.png')}/>
            <Text style={{...style.subTitle, color: '#3BD704', fontSize: 11}}>Estoque Disponível!</Text>
            <Text style={{...style.subTitle, fontFamily: 'Poppins_500Medium', fontSize: 16}}>Óculos</Text>
            <Text style={{...style.subTitle, color: '#B7B7B7'}}>Óculos de Proteção Lente Pro</Text>
            <TouchableOpacity onPress={() => {router.push('screens/novo-pedido/carrinho')}}>
              <Text style={style.button}>Fazer pedido +</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={style.container}>
          <View style={style.card}>
            <Image style={style.img} source={require('../../../assets/images/bota42.png')}/>
            <Text style={{...style.subTitle, color: '#3BD704', fontSize: 11}}>Estoque Disponível!</Text>
            <Text style={{...style.subTitle, fontFamily: 'Poppins_500Medium', fontSize: 16}}>Bota</Text>
            <Text style={{...style.subTitle, color: '#B7B7B7'}}>Bota EPI Boraracha Tamanho 42</Text>
            <TouchableOpacity onPress={() => {router.push('screens/novo-pedido/carrinho')}}>
              <Text style={style.button}>Fazer pedido +</Text>
            </TouchableOpacity>
          </View>
        </View>
        
      </View>
      <Navbar/>
    </View>
  )
}

const style = StyleSheet.create({
  row1: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    flexWrap: 'wrap'
  },
  container: {
    height: '50%',
    width: '43%',
    backgroundColor: '#fff',
    marginHorizontal: 10,
    marginVertical: 10,
    borderRadius: 10,
    elevation: 7,
    shadowColor: '#C7C7C7',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 7,
  },
  card: {
    display: 'flex',
    flexDirection: 'column'
  },
  img: {
    marginLeft: 12,
    marginVertical: 12,
  },
  subTitle: {
    paddingLeft: 12,
    fontFamily: 'Poppins_400Regular'
  },
  button: {
    paddingTop: 10,
    paddingLeft: 12,
    fontFamily: 'Poppins_500Medium',
    color: '#FFC857'
  }
})

export default novoPedido