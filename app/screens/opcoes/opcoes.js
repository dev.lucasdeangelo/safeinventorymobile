import { StyleSheet, Text, View, TouchableOpacity, Image, Switch } from 'react-native'
import React, { useState } from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import { useRouter } from 'expo-router'
import { useFonts, Poppins_600SemiBold, Poppins_500Medium, Poppins_400Regular,  } from '@expo-google-fonts/poppins'

const Opcoes = () => {
  const [isEnabled, setIsEnabled] = useState(false);
  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  const router = useRouter()
  const [fontLoaded] = useFonts({
    Poppins_600SemiBold,
    Poppins_500Medium,
    Poppins_400Regular
})
  return (
    <View style={{ backgroundColor: '#F3F3F3', height: '90%' }}>
      <TopNavbar titulo='Opções'/>
      <View style={styles.container}> 
        <View style={styles.card}>
          <Image style={styles.img} source={require('../../../assets/icons/alertas.png')}/>
          <View style={styles.content}>
            <Text style={styles.h2}>Notificações</Text>           
          </View>
          <View style={{paddingRight: 26}}>
            <Switch
            trackColor={{false: '#767577', true: '#81b0ff'}}
            thumbColor={isEnabled ? '#f5dd4b' : '#f4f3f4'}
            ios_backgroundColor="#3e3e3e"
            onValueChange={toggleSwitch}
            value={isEnabled}/>
          </View>
        </View>
      </View>
      <Navbar/>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    height: '100%',
    backgroundColor: '#fff',
    marginHorizontal: 15,
    marginVertical: 10,
    borderRadius: 10,
    elevation: 7,
    shadowColor: '#C7C7C7',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 7,
  },
  card: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingTop: 10
  },
  img: {
    marginLeft: 25,
    marginTop: 10
  },
  content: {
    paddingRight: 35,
    marginTop: 10,
  },
  h2: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: "Poppins_600SemiBold",
  },
})

export default Opcoes