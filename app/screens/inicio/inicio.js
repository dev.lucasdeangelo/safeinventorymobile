import { View, Text, StyleSheet, Image } from 'react-native'
import React, { useState, useEffect } from 'react'
import Navbar from '../../components/navbar'
import TopNavbar from '../../components/top-navbar'
import * as Font from 'expo-font';

const Inicio = () => {
  const [fontLoaded, setFontLoaded] = useState(false);
    useEffect(() => {
    async function loadFonts() {
      await Font.loadAsync({
        'Poppins-SemiBold': require('../../../assets/fonts/Poppins-SemiBold.ttf'),
        'Poppins-Regular': require('../../../assets/fonts/Poppins-Regular.ttf'),
      });
      
      setFontLoaded(true);
    }

    loadFonts();
  }, []);
  return (
    <View style= {{ backgroundColor: '#F3F3F3', height: '100%' }}>
        <TopNavbar titulo='Início'/>
        <View style={styles.container}>
        {fontLoaded && (
          <><Text style={styles.h3}>Seus Equipamentos</Text></>
        )}
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>01</Text>
              </>
            )}
            </View>
          </View>
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>02</Text>
              </>
            )}
            </View>
          </View>
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>03</Text>
              </>
            )}
            </View>
          </View>
          <View style={styles.card}>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>04</Text>
              </>
            )}
            </View>
          </View>
        </View>
        <View style={styles.container}>
        {fontLoaded && (
          <>
          <Text style={styles.h3}>Suas Trocas</Text>
          </>
        )}
          <View style={styles.card}>
            <input type='checkbox'/>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>01</Text>
              </>
            )}
            </View>
          </View>
          <View style={styles.card}>
          <input type='checkbox'/>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>02</Text>
              </>
            )}
            </View>
          </View>
          <View style={styles.card}>
            <input type='checkbox'/>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>03</Text>
              </>
            )}
            </View>
          </View>
          <View style={styles.card}>
            <input type='checkbox'/>
            <Image style={styles.img} source={require('../../../assets/images/bota.png')}/>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Botas de Borracha 43</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>Botas</Text>
              </>
            )}
            </View>
            <View style={styles.subTitle}>
            {fontLoaded && (
              <>
              <Text style={styles.txt}>Código</Text>
              <Text style={{ ...styles.txt, color: '#BFBFBF'}}>04</Text>
              </>
            )}
            </View>
          </View>
        </View>
        <Navbar/>
    </View>
  )
}
const styles = StyleSheet.create({
  container: {
    backgroundColor: '#fff',
    margin: 15,
    borderRadius: 5,
    padding: 10,
    elevation: 7,
    shadowColor: '#C7C7C7',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.7,
    shadowRadius: 7,
  },
  h3: {
    paddingBottom: 10,
    fontSize: 20,
    fontFamily: "Poppins-SemiBold",
  },
  card: {
    display: 'flex',
    flexDirection: 'row'
  },
  subTitle: {
    paddingLeft: 22,
    paddingBottom: 12
  },
  txt: {
    fontSize: 14,
    fontFamily: "Poppins-Regular",
  },
})

export default Inicio